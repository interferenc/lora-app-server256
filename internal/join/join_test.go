package join

import (
	"fmt"
	"testing"

	"github.com/gofrs/uuid"

	"gitlab.com/interferenc/lora-app-server256/internal/config"
	"gitlab.com/interferenc/lora-app-server256/internal/storage"
	"gitlab.com/interferenc/lora-app-server256/internal/test"
	"gitlab.com/interferenc/lora-app-server256/internal/test/testhandler"
	"gitlab.com/interferenc/lorawan256"
	"gitlab.com/interferenc/lorawan256/backend"
	. "github.com/smartystreets/goconvey/convey"
)

func TestJoin(t *testing.T) {
	conf := test.GetConfig()
	db, err := storage.OpenDatabase(conf.PostgresDSN)
	if err != nil {
		t.Fatal(err)
	}

	p := storage.NewRedisPool(conf.RedisURL, 10, 0)

	config.C.PostgreSQL.DB = db
	config.C.Redis.Pool = p

	Convey("Given a clean database with node", t, func() {
		test.MustResetDB(config.C.PostgreSQL.DB)
		test.MustFlushRedis(p)

		nsClient := test.NewNetworkServerClient()
		config.C.NetworkServer.Pool = test.NewNetworkServerPool(nsClient)

		h := testhandler.NewTestHandler()
		config.C.ApplicationServer.Integration.Handler = h

		org := storage.Organization{
			Name: "test-org",
		}
		So(storage.CreateOrganization(config.C.PostgreSQL.DB, &org), ShouldBeNil)

		n := storage.NetworkServer{
			Name:   "test-ns",
			Server: "test-ns:1234",
		}
		So(storage.CreateNetworkServer(config.C.PostgreSQL.DB, &n), ShouldBeNil)

		sp := storage.ServiceProfile{
			OrganizationID:  org.ID,
			NetworkServerID: n.ID,
			Name:            "test-sp",
		}
		So(storage.CreateServiceProfile(config.C.PostgreSQL.DB, &sp), ShouldBeNil)
		spID, err := uuid.FromBytes(sp.ServiceProfile.Id)
		So(err, ShouldBeNil)

		dp := storage.DeviceProfile{
			OrganizationID:  org.ID,
			NetworkServerID: n.ID,
			Name:            "test-dp",
		}
		So(storage.CreateDeviceProfile(config.C.PostgreSQL.DB, &dp), ShouldBeNil)
		dpID, err := uuid.FromBytes(dp.DeviceProfile.Id)
		So(err, ShouldBeNil)

		app := storage.Application{
			OrganizationID:   org.ID,
			ServiceProfileID: spID,
			Name:             "test-app",
		}
		So(storage.CreateApplication(config.C.PostgreSQL.DB, &app), ShouldBeNil)

		d := storage.Device{
			ApplicationID:   app.ID,
			Name:            "test-device",
			DevEUI:          lorawan.EUI64{1, 2, 3, 4, 5, 6, 7, 8},
			DeviceProfileID: dpID,
		}
		So(storage.CreateDevice(config.C.PostgreSQL.DB, &d), ShouldBeNil)

		dk := storage.DeviceKeys{
			DevEUI:    d.DevEUI,
			NwkKey:    lorawan.AES256Key{1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8},
			JoinNonce: 65535,
		}
		So(storage.CreateDeviceKeys(config.C.PostgreSQL.DB, &dk), ShouldBeNil)

		cFList := lorawan.CFList{
			CFListType: lorawan.CFListChannel,
			Payload: &lorawan.CFListChannelPayload{
				Channels: [5]uint32{
					868700000,
					868900000,
				},
			},
		}
		cFListB, err := cFList.MarshalBinary()
		So(err, ShouldBeNil)

		config.C.JoinServer.KEK.ASKEKLabel = ""
		config.C.JoinServer.KEK.Set = nil

		Convey("Given a set of tests for join-request", func() {
			validJRPHY := lorawan.PHYPayload{
				MHDR: lorawan.MHDR{
					MType: lorawan.JoinRequest,
					Major: lorawan.LoRaWANR1,
				},
				MACPayload: &lorawan.JoinRequestPayload{
					DevEUI:   d.DevEUI,
					JoinEUI:  lorawan.EUI64{8, 7, 6, 5, 4, 3, 2, 1},
					DevNonce: 258,
				},
			}
			So(validJRPHY.SetUplinkJoinMIC(dk.NwkKey), ShouldBeNil)
			validJRPHYBytes, err := validJRPHY.MarshalBinary()
			So(err, ShouldBeNil)

			invalidMICJRPHY := lorawan.PHYPayload{
				MHDR: lorawan.MHDR{
					MType: lorawan.JoinRequest,
					Major: lorawan.LoRaWANR1,
				},
				MACPayload: &lorawan.JoinRequestPayload{
					DevEUI:   d.DevEUI,
					JoinEUI:  lorawan.EUI64{8, 7, 6, 5, 4, 3, 2, 1},
					DevNonce: 258,
				},
			}
			So(invalidMICJRPHY.SetUplinkJoinMIC(lorawan.AES256Key{}), ShouldBeNil)
			invalidMICJRPHYBytes, err := invalidMICJRPHY.MarshalBinary()
			So(err, ShouldBeNil)

			validJAPHY := lorawan.PHYPayload{
				MHDR: lorawan.MHDR{
					MType: lorawan.JoinAccept,
					Major: lorawan.LoRaWANR1,
				},
				MACPayload: &lorawan.JoinAcceptPayload{
					JoinNonce: 65536,
					HomeNetID: lorawan.NetID{1, 2, 3},
					DevAddr:   lorawan.DevAddr{1, 2, 3, 4},
					DLSettings: lorawan.DLSettings{
						RX2DataRate: 5,
						RX1DROffset: 1,
					},
					RXDelay: 1,
					CFList: &lorawan.CFList{
						CFListType: lorawan.CFListChannel,
						Payload: &lorawan.CFListChannelPayload{
							Channels: [5]uint32{
								868700000,
								868900000,
							},
						},
					},
				},
			}
			So(validJAPHY.SetDownlinkJoinMIC(lorawan.JoinRequestType, lorawan.EUI64{8, 7, 6, 5, 4, 3, 2, 1}, 258, dk.NwkKey), ShouldBeNil)
			So(validJAPHY.EncryptJoinAcceptPayload(dk.NwkKey), ShouldBeNil)
			validJAPHYBytes, err := validJAPHY.MarshalBinary()
			So(err, ShouldBeNil)

			validJAPHYLW11 := lorawan.PHYPayload{
				MHDR: lorawan.MHDR{
					MType: lorawan.JoinAccept,
					Major: lorawan.LoRaWANR1,
				},
				MACPayload: &lorawan.JoinAcceptPayload{
					JoinNonce: 65536,
					HomeNetID: lorawan.NetID{1, 2, 3},
					DevAddr:   lorawan.DevAddr{1, 2, 3, 4},
					DLSettings: lorawan.DLSettings{
						OptNeg:      true,
						RX2DataRate: 5,
						RX1DROffset: 1,
					},
					RXDelay: 1,
					CFList: &lorawan.CFList{
						CFListType: lorawan.CFListChannel,
						Payload: &lorawan.CFListChannelPayload{
							Channels: [5]uint32{
								868700000,
								868900000,
							},
						},
					},
				},
			}
			jsIntKey, err := getJSIntKey(dk.NwkKey, d.DevEUI)
			So(err, ShouldBeNil)
			So(validJAPHYLW11.SetDownlinkJoinMIC(lorawan.JoinRequestType, lorawan.EUI64{8, 7, 6, 5, 4, 3, 2, 1}, 258, jsIntKey), ShouldBeNil)
			So(validJAPHYLW11.EncryptJoinAcceptPayload(dk.NwkKey), ShouldBeNil)
			validJAPHYLW11Bytes, err := validJAPHYLW11.MarshalBinary()
			So(err, ShouldBeNil)

			tests := []struct {
				Name            string
				PreRun          func() error
				RequestPayload  backend.JoinReqPayload
				ExpectedPayload backend.JoinAnsPayload
			}{
				{
					Name: "valid join-request (LoRaWAN 1.0)",
					RequestPayload: backend.JoinReqPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "010203",
							ReceiverID:      "0807060504030201",
							TransactionID:   1234,
							MessageType:     backend.JoinReq,
						},
						MACVersion: "1.0.2",
						PHYPayload: backend.HEXBytes(validJRPHYBytes),
						DevEUI:     d.DevEUI,
						DevAddr:    lorawan.DevAddr{1, 2, 3, 4},
						DLSettings: lorawan.DLSettings{
							RX2DataRate: 5,
							RX1DROffset: 1,
						},
						RxDelay: 1,
						CFList:  backend.HEXBytes(cFListB),
					},
					ExpectedPayload: backend.JoinAnsPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "0807060504030201",
							ReceiverID:      "010203",
							TransactionID:   1234,
							MessageType:     backend.JoinAns,
						},
						Result: backend.Result{
							ResultCode: backend.Success,
						},
						PHYPayload: backend.HEXBytes(validJAPHYBytes),
						NwkSKey: &backend.KeyEnvelope{
							AESKey: []byte{73, 245, 245, 208, 175, 92, 128, 254, 143, 135, 70, 146, 200, 5, 47, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						AppSKey: &backend.KeyEnvelope{
							AESKey: []byte{160, 193, 110, 124, 5, 194, 77, 173, 105, 126, 44, 80, 11, 79, 72, 209, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
					},
				},
				{
					Name: "valid join-request (LoRaWAN 1.1)",
					RequestPayload: backend.JoinReqPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "010203",
							ReceiverID:      "0807060504030201",
							TransactionID:   1234,
							MessageType:     backend.JoinReq,
						},
						MACVersion: "1.1.0",
						PHYPayload: backend.HEXBytes(validJRPHYBytes),
						DevEUI:     d.DevEUI,
						DevAddr:    lorawan.DevAddr{1, 2, 3, 4},
						DLSettings: lorawan.DLSettings{
							OptNeg:      true,
							RX2DataRate: 5,
							RX1DROffset: 1,
						},
						RxDelay: 1,
						CFList:  backend.HEXBytes(cFListB),
					},
					ExpectedPayload: backend.JoinAnsPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "0807060504030201",
							ReceiverID:      "010203",
							TransactionID:   1234,
							MessageType:     backend.JoinAns,
						},
						Result: backend.Result{
							ResultCode: backend.Success,
						},
						PHYPayload: backend.HEXBytes(validJAPHYLW11Bytes),
						FNwkSIntKey: &backend.KeyEnvelope{
							AESKey: []byte{44, 135, 87, 8, 144, 74, 72, 217, 241, 184, 92, 123, 98, 132, 252, 229, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						SNwkSIntKey: &backend.KeyEnvelope{
							AESKey: []byte{33, 198, 179, 71, 93, 184, 91, 166, 237, 237, 162, 221, 211, 226, 209, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						NwkSEncKey: &backend.KeyEnvelope{
							AESKey: []byte{122, 100, 170, 52, 78, 157, 198, 43, 74, 195, 3, 83, 32, 54, 48, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						AppSKey: &backend.KeyEnvelope{
							AESKey: []byte{90, 147, 89, 167, 131, 209, 160, 113, 172, 162, 57, 104, 222, 162, 4, 111, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
					},
				},
				{
					PreRun: func() error {
						config.C.JoinServer.KEK.ASKEKLabel = "lora-app-server"
						config.C.JoinServer.KEK.Set = []struct {
							Label string `mapstructure:"label"`
							KEK   string `mapstructure:"kek"`
						}{
							{
								Label: "010203",
								KEK:   "00000000000000000000000000000000",
							},
							{
								Label: "lora-app-server",
								KEK:   "00000000000000000000000000000000",
							},
						}

						return nil
					},
					Name: "valid join-request (LoRaWAN 1.1) with KEK",
					RequestPayload: backend.JoinReqPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "010203",
							ReceiverID:      "0807060504030201",
							TransactionID:   1234,
							MessageType:     backend.JoinReq,
						},
						MACVersion: "1.1.0",
						PHYPayload: backend.HEXBytes(validJRPHYBytes),
						DevEUI:     d.DevEUI,
						DevAddr:    lorawan.DevAddr{1, 2, 3, 4},
						DLSettings: lorawan.DLSettings{
							OptNeg:      true,
							RX2DataRate: 5,
							RX1DROffset: 1,
						},
						RxDelay: 1,
						CFList:  backend.HEXBytes(cFListB),
					},
					ExpectedPayload: backend.JoinAnsPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "0807060504030201",
							ReceiverID:      "010203",
							TransactionID:   1234,
							MessageType:     backend.JoinAns,
						},
						Result: backend.Result{
							ResultCode: backend.Success,
						},
						PHYPayload: backend.HEXBytes(validJAPHYLW11Bytes),
						FNwkSIntKey: &backend.KeyEnvelope{
							KEKLabel: "010203",
							AESKey:   []byte{253, 39, 194, 156, 20, 140, 85, 152, 190, 247, 127, 224, 110, 179, 38, 146, 168, 50, 75, 35, 126, 8, 224, 193, 89, 136, 254, 20, 6, 55, 105, 179, 185, 21, 129, 60, 169, 40, 65, 37},
						},
						SNwkSIntKey: &backend.KeyEnvelope{
							KEKLabel: "010203",
							AESKey:   []byte{187, 114, 236, 235, 167, 80, 7, 247, 74, 99, 21, 51, 35, 127, 198, 182, 111, 139, 43, 187, 92, 122, 70, 72, 173, 52, 160, 235, 95, 240, 91, 83, 21, 231, 78, 186, 78, 76, 91, 57},
						},
						NwkSEncKey: &backend.KeyEnvelope{
							KEKLabel: "010203",
							AESKey:   []byte{63, 172, 200, 52, 200, 163, 208, 14, 39, 162, 197, 164, 164, 21, 46, 85, 40, 223, 6, 101, 143, 112, 233, 73, 211, 144, 40, 73, 145, 168, 83, 236, 97, 25, 26, 16, 83, 199, 124, 24},
						},
						AppSKey: &backend.KeyEnvelope{
							KEKLabel: "lora-app-server",
							AESKey:   []byte{136, 74, 166, 62, 255, 150, 241, 45, 0, 10, 127, 53, 181, 70, 227, 108, 92, 191, 203, 236, 4, 20, 115, 69, 58, 25, 100, 160, 193, 242, 0, 125, 121, 46, 54, 109, 236, 59, 91, 33},
						},
					},
				},
				{
					Name: "join-request with invalid mic",
					RequestPayload: backend.JoinReqPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "010203",
							ReceiverID:      "0807060504030201",
							TransactionID:   1234,
							MessageType:     backend.JoinReq,
						},
						MACVersion: "1.0.2",
						PHYPayload: backend.HEXBytes(invalidMICJRPHYBytes),
						DevEUI:     d.DevEUI,
						DevAddr:    lorawan.DevAddr{1, 2, 3, 4},
						DLSettings: lorawan.DLSettings{
							RX2DataRate: 5,
							RX1DROffset: 1,
						},
						RxDelay: 1,
						CFList:  backend.HEXBytes(cFListB),
					},
					ExpectedPayload: backend.JoinAnsPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "0807060504030201",
							ReceiverID:      "010203",
							TransactionID:   1234,
							MessageType:     backend.JoinAns,
						},
						Result: backend.Result{
							ResultCode:  backend.MICFailed,
							Description: "invalid mic",
						},
					},
				},
				{
					Name: "join-request for unknown device",
					RequestPayload: backend.JoinReqPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "010203",
							ReceiverID:      "0807060504030201",
							TransactionID:   1234,
							MessageType:     backend.JoinReq,
						},
						MACVersion: "1.0.2",
						PHYPayload: backend.HEXBytes(validJRPHYBytes),
						DevEUI:     lorawan.EUI64{1, 1, 1, 1, 1, 1, 1, 1},
						DevAddr:    lorawan.DevAddr{1, 2, 3, 4},
						DLSettings: lorawan.DLSettings{
							RX2DataRate: 5,
							RX1DROffset: 1,
						},
						RxDelay: 1,
						CFList:  backend.HEXBytes(cFListB),
					},
					ExpectedPayload: backend.JoinAnsPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "0807060504030201",
							ReceiverID:      "010203",
							TransactionID:   1234,
							MessageType:     backend.JoinAns,
						},
						Result: backend.Result{
							ResultCode:  backend.UnknownDevEUI,
							Description: "get device-keys error: object does not exist",
						},
					},
				},
				{
					Name: "join-request for device without keys",
					PreRun: func() error {
						return storage.DeleteDeviceKeys(config.C.PostgreSQL.DB, d.DevEUI)
					},
					RequestPayload: backend.JoinReqPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "010203",
							ReceiverID:      "0807060504030201",
							TransactionID:   1234,
							MessageType:     backend.JoinReq,
						},
						MACVersion: "1.0.2",
						PHYPayload: backend.HEXBytes(validJRPHYBytes),
						DevEUI:     d.DevEUI,
						DevAddr:    lorawan.DevAddr{1, 2, 3, 4},
						DLSettings: lorawan.DLSettings{
							RX2DataRate: 5,
							RX1DROffset: 1,
						},
						RxDelay: 1,
						CFList:  backend.HEXBytes(cFListB),
					},
					ExpectedPayload: backend.JoinAnsPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "0807060504030201",
							ReceiverID:      "010203",
							TransactionID:   1234,
							MessageType:     backend.JoinAns,
						},
						Result: backend.Result{
							ResultCode:  backend.UnknownDevEUI,
							Description: "get device-keys error: object does not exist",
						},
					},
				},
			}

			for i, test := range tests {
				Convey(fmt.Sprintf("Testing: %s [%d]", test.Name, i), func() {
					if test.PreRun != nil {
						So(test.PreRun(), ShouldBeNil)
					}

					ans := HandleJoinRequest(test.RequestPayload)
					So(ans, ShouldResemble, test.ExpectedPayload)
				})
			}
		})

		Convey("Given a set of tests for rejoin-request", func() {
			jsIntKey, err := getJSIntKey(dk.NwkKey, d.DevEUI)
			So(err, ShouldBeNil)
			jsEncKey, err := getJSEncKey(dk.NwkKey, d.DevEUI)
			So(err, ShouldBeNil)

			rj0PHY := lorawan.PHYPayload{
				MHDR: lorawan.MHDR{
					MType: lorawan.RejoinRequest,
					Major: lorawan.LoRaWANR1,
				},
				MACPayload: &lorawan.RejoinRequestType02Payload{
					RejoinType: lorawan.RejoinRequestType0,
					NetID:      lorawan.NetID{1, 2, 3},
					DevEUI:     d.DevEUI,
					RJCount0:   123,
				},
			}
			// no need to set the MIC as it is not validated by the js
			rj0PHYBytes, err := rj0PHY.MarshalBinary()
			So(err, ShouldBeNil)

			rj1PHY := lorawan.PHYPayload{
				MHDR: lorawan.MHDR{
					MType: lorawan.RejoinRequest,
					Major: lorawan.LoRaWANR1,
				},
				MACPayload: &lorawan.RejoinRequestType1Payload{
					RejoinType: lorawan.RejoinRequestType1,
					JoinEUI:    lorawan.EUI64{8, 7, 6, 5, 4, 3, 2, 1},
					DevEUI:     d.DevEUI,
					RJCount1:   123,
				},
			}
			So(rj1PHY.SetUplinkJoinMIC(jsIntKey), ShouldBeNil)
			rj1PHYBytes, err := rj1PHY.MarshalBinary()
			So(err, ShouldBeNil)

			rj2PHY := lorawan.PHYPayload{
				MHDR: lorawan.MHDR{
					MType: lorawan.RejoinRequest,
					Major: lorawan.LoRaWANR1,
				},
				MACPayload: &lorawan.RejoinRequestType02Payload{
					RejoinType: lorawan.RejoinRequestType2,
					NetID:      lorawan.NetID{1, 2, 3},
					DevEUI:     d.DevEUI,
					RJCount0:   123,
				},
			}
			// no need to set the MIC as it is not validated by the js
			rj2PHYBytes, err := rj2PHY.MarshalBinary()
			So(err, ShouldBeNil)

			ja0PHY := lorawan.PHYPayload{
				MHDR: lorawan.MHDR{
					MType: lorawan.JoinAccept,
					Major: lorawan.LoRaWANR1,
				},
				MACPayload: &lorawan.JoinAcceptPayload{
					JoinNonce: lorawan.JoinNonce(dk.JoinNonce + 1),
					HomeNetID: lorawan.NetID{1, 2, 3},
					DevAddr:   lorawan.DevAddr{1, 2, 3, 4},
					DLSettings: lorawan.DLSettings{
						OptNeg:      true,
						RX2DataRate: 5,
						RX1DROffset: 1,
					},
					RXDelay: 1,
					CFList: &lorawan.CFList{
						CFListType: lorawan.CFListChannel,
						Payload: &lorawan.CFListChannelPayload{
							Channels: [5]uint32{
								868700000,
								868900000,
							},
						},
					},
				},
			}
			So(ja0PHY.SetDownlinkJoinMIC(lorawan.RejoinRequestType0, lorawan.EUI64{8, 7, 6, 5, 4, 3, 2, 1}, 123, jsIntKey), ShouldBeNil)
			So(ja0PHY.EncryptJoinAcceptPayload(jsEncKey), ShouldBeNil)
			ja0PHYBytes, err := ja0PHY.MarshalBinary()
			So(err, ShouldBeNil)

			ja1PHY := lorawan.PHYPayload{
				MHDR: lorawan.MHDR{
					MType: lorawan.JoinAccept,
					Major: lorawan.LoRaWANR1,
				},
				MACPayload: &lorawan.JoinAcceptPayload{
					JoinNonce: lorawan.JoinNonce(dk.JoinNonce + 1),
					HomeNetID: lorawan.NetID{1, 2, 3},
					DevAddr:   lorawan.DevAddr{1, 2, 3, 4},
					DLSettings: lorawan.DLSettings{
						OptNeg:      true,
						RX2DataRate: 5,
						RX1DROffset: 1,
					},
					RXDelay: 1,
					CFList: &lorawan.CFList{
						CFListType: lorawan.CFListChannel,
						Payload: &lorawan.CFListChannelPayload{
							Channels: [5]uint32{
								868700000,
								868900000,
							},
						},
					},
				},
			}
			So(ja1PHY.SetDownlinkJoinMIC(lorawan.RejoinRequestType1, lorawan.EUI64{8, 7, 6, 5, 4, 3, 2, 1}, 123, jsIntKey), ShouldBeNil)
			So(ja1PHY.EncryptJoinAcceptPayload(jsEncKey), ShouldBeNil)
			ja1PHYBytes, err := ja1PHY.MarshalBinary()
			So(err, ShouldBeNil)

			ja2PHY := lorawan.PHYPayload{
				MHDR: lorawan.MHDR{
					MType: lorawan.JoinAccept,
					Major: lorawan.LoRaWANR1,
				},
				MACPayload: &lorawan.JoinAcceptPayload{
					JoinNonce: lorawan.JoinNonce(dk.JoinNonce + 1),
					HomeNetID: lorawan.NetID{1, 2, 3},
					DevAddr:   lorawan.DevAddr{1, 2, 3, 4},
					DLSettings: lorawan.DLSettings{
						OptNeg:      true,
						RX2DataRate: 5,
						RX1DROffset: 1,
					},
					RXDelay: 1,
				},
			}
			So(ja2PHY.SetDownlinkJoinMIC(lorawan.RejoinRequestType2, lorawan.EUI64{8, 7, 6, 5, 4, 3, 2, 1}, 123, jsIntKey), ShouldBeNil)
			So(ja2PHY.EncryptJoinAcceptPayload(jsEncKey), ShouldBeNil)
			ja2PHYBytes, err := ja2PHY.MarshalBinary()
			So(err, ShouldBeNil)

			tests := []struct {
				Name            string
				PreRun          func() error
				RequestPayload  backend.RejoinReqPayload
				ExpectedPayload backend.RejoinAnsPayload
			}{
				{
					Name: "valid rejoin-request type 0",
					RequestPayload: backend.RejoinReqPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "010203",
							ReceiverID:      "0807060504030201",
							TransactionID:   1234,
							MessageType:     backend.RejoinReq,
						},
						MACVersion: "1.1.0",
						PHYPayload: backend.HEXBytes(rj0PHYBytes),
						DevEUI:     d.DevEUI,
						DevAddr:    lorawan.DevAddr{1, 2, 3, 4},
						DLSettings: lorawan.DLSettings{
							OptNeg:      true,
							RX2DataRate: 5,
							RX1DROffset: 1,
						},
						RxDelay: 1,
						CFList:  backend.HEXBytes(cFListB),
					},
					ExpectedPayload: backend.RejoinAnsPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "0807060504030201",
							ReceiverID:      "010203",
							TransactionID:   1234,
							MessageType:     backend.RejoinAns,
						},
						Result: backend.Result{
							ResultCode: backend.Success,
						},
						PHYPayload: backend.HEXBytes(ja0PHYBytes),
						SNwkSIntKey: &backend.KeyEnvelope{
							AESKey: []byte{149, 120, 5, 96, 155, 128, 176, 208, 48, 95, 243, 111, 106, 151, 199, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						FNwkSIntKey: &backend.KeyEnvelope{
							AESKey: []byte{116, 107, 49, 181, 151, 129, 9, 167, 149, 189, 41, 47, 222, 74, 230, 204, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						NwkSEncKey: &backend.KeyEnvelope{
							AESKey: []byte{84, 195, 55, 234, 72, 154, 106, 145, 52, 17, 173, 15, 100, 144, 102, 197, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						AppSKey: &backend.KeyEnvelope{
							AESKey: []byte{25, 22, 171, 213, 146, 40, 141, 222, 213, 179, 62, 103, 11, 138, 46, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
					},
				},
				{
					Name: "valid rejoin-request type 1",
					RequestPayload: backend.RejoinReqPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "010203",
							ReceiverID:      "0807060504030201",
							TransactionID:   1234,
							MessageType:     backend.RejoinReq,
						},
						MACVersion: "1.1.0",
						PHYPayload: backend.HEXBytes(rj1PHYBytes),
						DevEUI:     d.DevEUI,
						DevAddr:    lorawan.DevAddr{1, 2, 3, 4},
						DLSettings: lorawan.DLSettings{
							OptNeg:      true,
							RX2DataRate: 5,
							RX1DROffset: 1,
						},
						RxDelay: 1,
						CFList:  backend.HEXBytes(cFListB),
					},
					ExpectedPayload: backend.RejoinAnsPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "0807060504030201",
							ReceiverID:      "010203",
							TransactionID:   1234,
							MessageType:     backend.RejoinAns,
						},
						Result: backend.Result{
							ResultCode: backend.Success,
						},
						PHYPayload: backend.HEXBytes(ja1PHYBytes),
						SNwkSIntKey: &backend.KeyEnvelope{
							AESKey: []byte{149, 120, 5, 96, 155, 128, 176, 208, 48, 95, 243, 111, 106, 151, 199, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						FNwkSIntKey: &backend.KeyEnvelope{
							AESKey: []byte{116, 107, 49, 181, 151, 129, 9, 167, 149, 189, 41, 47, 222, 74, 230, 204, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						NwkSEncKey: &backend.KeyEnvelope{
							AESKey: []byte{84, 195, 55, 234, 72, 154, 106, 145, 52, 17, 173, 15, 100, 144, 102, 197, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						AppSKey: &backend.KeyEnvelope{
							AESKey: []byte{25, 22, 171, 213, 146, 40, 141, 222, 213, 179, 62, 103, 11, 138, 46, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
					},
				},
				{
					Name: "valid rejoin-request type 2",
					RequestPayload: backend.RejoinReqPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "010203",
							ReceiverID:      "0807060504030201",
							TransactionID:   1234,
							MessageType:     backend.RejoinReq,
						},
						MACVersion: "1.1.0",
						PHYPayload: backend.HEXBytes(rj2PHYBytes),
						DevEUI:     d.DevEUI,
						DevAddr:    lorawan.DevAddr{1, 2, 3, 4},
						DLSettings: lorawan.DLSettings{
							OptNeg:      true,
							RX2DataRate: 5,
							RX1DROffset: 1,
						},
						RxDelay: 1,
					},
					ExpectedPayload: backend.RejoinAnsPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "0807060504030201",
							ReceiverID:      "010203",
							TransactionID:   1234,
							MessageType:     backend.RejoinAns,
						},
						Result: backend.Result{
							ResultCode: backend.Success,
						},
						PHYPayload: backend.HEXBytes(ja2PHYBytes),
						SNwkSIntKey: &backend.KeyEnvelope{
							AESKey: []byte{149, 120, 5, 96, 155, 128, 176, 208, 48, 95, 243, 111, 106, 151, 199, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						FNwkSIntKey: &backend.KeyEnvelope{
							AESKey: []byte{116, 107, 49, 181, 151, 129, 9, 167, 149, 189, 41, 47, 222, 74, 230, 204, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						NwkSEncKey: &backend.KeyEnvelope{
							AESKey: []byte{84, 195, 55, 234, 72, 154, 106, 145, 52, 17, 173, 15, 100, 144, 102, 197, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						AppSKey: &backend.KeyEnvelope{
							AESKey: []byte{25, 22, 171, 213, 146, 40, 141, 222, 213, 179, 62, 103, 11, 138, 46, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
					},
				},
			}

			for i, test := range tests {
				Convey(fmt.Sprintf("Testing: %s [%d]", test.Name, i), func() {
					if test.PreRun != nil {
						So(test.PreRun(), ShouldBeNil)
					}

					ans := HandleRejoinRequest(test.RequestPayload)
					So(ans, ShouldResemble, test.ExpectedPayload)
				})
			}
		})
	})
}
