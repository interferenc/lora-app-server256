package api

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gofrs/uuid"

	"gitlab.com/interferenc/lora-app-server256/internal/config"
	"gitlab.com/interferenc/lora-app-server256/internal/test/testhandler"

	"gitlab.com/interferenc/lora-app-server256/internal/storage"
	"gitlab.com/interferenc/lora-app-server256/internal/test"
	"gitlab.com/interferenc/lorawan256"
	"gitlab.com/interferenc/lorawan256/backend"
	. "github.com/smartystreets/goconvey/convey"
)

func TestJoinServerAPI(t *testing.T) {
	conf := test.GetConfig()
	db, err := storage.OpenDatabase(conf.PostgresDSN)
	if err != nil {
		t.Fatal(err)
	}
	config.C.PostgreSQL.DB = db
	config.C.Redis.Pool = storage.NewRedisPool(conf.RedisURL, 10, 0)

	Convey("Given a clean database with a device", t, func() {
		test.MustResetDB(config.C.PostgreSQL.DB)
		test.MustFlushRedis(config.C.Redis.Pool)

		nsClient := test.NewNetworkServerClient()
		config.C.NetworkServer.Pool = test.NewNetworkServerPool(nsClient)

		h := testhandler.NewTestHandler()
		config.C.ApplicationServer.Integration.Handler = h

		org := storage.Organization{
			Name: "test-org",
		}
		So(storage.CreateOrganization(config.C.PostgreSQL.DB, &org), ShouldBeNil)

		n := storage.NetworkServer{
			Name:   "test-ns",
			Server: "test-ns:1234",
		}
		So(storage.CreateNetworkServer(config.C.PostgreSQL.DB, &n), ShouldBeNil)

		sp := storage.ServiceProfile{
			OrganizationID:  org.ID,
			NetworkServerID: n.ID,
			Name:            "test-sp",
		}
		So(storage.CreateServiceProfile(config.C.PostgreSQL.DB, &sp), ShouldBeNil)
		spID, err := uuid.FromBytes(sp.ServiceProfile.Id)
		So(err, ShouldBeNil)

		dp := storage.DeviceProfile{
			OrganizationID:  org.ID,
			NetworkServerID: n.ID,
			Name:            "test-dp",
		}
		So(storage.CreateDeviceProfile(config.C.PostgreSQL.DB, &dp), ShouldBeNil)
		dpID, err := uuid.FromBytes(dp.DeviceProfile.Id)
		So(err, ShouldBeNil)

		app := storage.Application{
			OrganizationID:   org.ID,
			ServiceProfileID: spID,
			Name:             "test-app",
		}
		So(storage.CreateApplication(config.C.PostgreSQL.DB, &app), ShouldBeNil)

		d := storage.Device{
			ApplicationID:   app.ID,
			Name:            "test-device",
			DevEUI:          lorawan.EUI64{1, 2, 3, 4, 5, 6, 7, 8},
			DeviceProfileID: dpID,
		}
		So(storage.CreateDevice(config.C.PostgreSQL.DB, &d), ShouldBeNil)

		dk := storage.DeviceKeys{
			DevEUI:    d.DevEUI,
			NwkKey:    lorawan.AES256Key{1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8},
			JoinNonce: 65535,
		}
		So(storage.CreateDeviceKeys(config.C.PostgreSQL.DB, &dk), ShouldBeNil)

		Convey("Given a test-server", func() {
			api := JoinServerAPI{}
			server := httptest.NewServer(&api)
			defer server.Close()

			cFList := lorawan.CFList{
				CFListType: lorawan.CFListChannel,
				Payload: &lorawan.CFListChannelPayload{
					Channels: [5]uint32{
						868700000,
						868900000,
					},
				},
			}
			cFListB, err := cFList.MarshalBinary()
			So(err, ShouldBeNil)

			Convey("When making a JoinReq call", func() {
				jrPHY := lorawan.PHYPayload{
					MHDR: lorawan.MHDR{
						MType: lorawan.JoinRequest,
						Major: lorawan.LoRaWANR1,
					},
					MACPayload: &lorawan.JoinRequestPayload{
						DevEUI:   d.DevEUI,
						JoinEUI:  lorawan.EUI64{8, 7, 6, 5, 4, 3, 2, 1},
						DevNonce: 258,
					},
				}
				So(jrPHY.SetUplinkJoinMIC(dk.NwkKey), ShouldBeNil)
				jrPHYBytes, err := jrPHY.MarshalBinary()
				So(err, ShouldBeNil)

				jaPHY := lorawan.PHYPayload{
					MHDR: lorawan.MHDR{
						MType: lorawan.JoinAccept,
						Major: lorawan.LoRaWANR1,
					},
					MACPayload: &lorawan.JoinAcceptPayload{
						JoinNonce: 65536,
						HomeNetID: lorawan.NetID{1, 2, 3},
						DevAddr:   lorawan.DevAddr{1, 2, 3, 4},
						DLSettings: lorawan.DLSettings{
							RX2DataRate: 5,
							RX1DROffset: 1,
						},
						RXDelay: 1,
						CFList: &lorawan.CFList{
							CFListType: lorawan.CFListChannel,
							Payload: &lorawan.CFListChannelPayload{
								Channels: [5]uint32{
									868700000,
									868900000,
								},
							},
						},
					},
				}
				So(jaPHY.SetDownlinkJoinMIC(lorawan.JoinRequestType, lorawan.EUI64{8, 7, 6, 5, 4, 3, 2, 1}, 513, dk.NwkKey), ShouldBeNil)
				So(jaPHY.EncryptJoinAcceptPayload(dk.NwkKey), ShouldBeNil)
				jaPHYBytes, err := jaPHY.MarshalBinary()
				So(err, ShouldBeNil)
				So(jaPHYBytes, ShouldResemble, []uint8{32, 210, 53, 36, 175, 187, 34, 168, 138, 4, 25, 254, 178, 35, 87, 59, 12, 116, 99, 175, 219, 2, 220, 148, 173, 178, 40, 177, 223, 96, 90, 164, 7})

				joinReqPayload := backend.JoinReqPayload{
					BasePayload: backend.BasePayload{
						ProtocolVersion: backend.ProtocolVersion1_0,
						SenderID:        "010203",
						ReceiverID:      "0807060504030201",
						TransactionID:   1234,
						MessageType:     backend.JoinReq,
					},
					MACVersion: "1.0.2",
					PHYPayload: backend.HEXBytes(jrPHYBytes),
					DevEUI:     d.DevEUI,
					DevAddr:    lorawan.DevAddr{1, 2, 3, 4},
					DLSettings: lorawan.DLSettings{
						RX2DataRate: 5,
						RX1DROffset: 1,
					},
					RxDelay: 1,
					CFList:  backend.HEXBytes(cFListB),
				}
				joinReqPayloadJSON, err := json.Marshal(joinReqPayload)
				So(err, ShouldBeNil)

				req, err := http.NewRequest("POST", server.URL, bytes.NewReader(joinReqPayloadJSON))
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, http.StatusOK)

				Convey("Then the expected response is returned", func() {
					var joinAnsPayload backend.JoinAnsPayload
					So(json.NewDecoder(resp.Body).Decode(&joinAnsPayload), ShouldBeNil)
					So(joinAnsPayload, ShouldResemble, backend.JoinAnsPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "0807060504030201",
							ReceiverID:      "010203",
							TransactionID:   1234,
							MessageType:     backend.JoinAns,
						},
						Result: backend.Result{
							ResultCode: backend.Success,
						},
						PHYPayload: backend.HEXBytes(jaPHYBytes),
						NwkSKey: &backend.KeyEnvelope{
							AESKey: []byte{73, 245, 245, 208, 175, 92, 128, 254, 143, 135, 70, 146, 200, 5, 47, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						AppSKey: &backend.KeyEnvelope{
							AESKey: []byte{160, 193, 110, 124, 5, 194, 77, 173, 105, 126, 44, 80, 11, 79, 72, 209, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
					})
				})

				Convey("Then the expected keys are stored", func() {
					dk, err := storage.GetDeviceKeys(db, d.DevEUI)
					So(err, ShouldBeNil)

					So(dk.JoinNonce, ShouldEqual, 65536)
				})
			})

			Convey("When making a RejoinReq call", func() {
				rjPHY := lorawan.PHYPayload{
					MHDR: lorawan.MHDR{
						MType: lorawan.RejoinRequest,
						Major: lorawan.LoRaWANR1,
					},
					MACPayload: &lorawan.RejoinRequestType02Payload{
						RejoinType: lorawan.RejoinRequestType2,
						NetID:      lorawan.NetID{1, 2, 3},
						DevEUI:     d.DevEUI,
						RJCount0:   123,
					},
				}
				// no need to set the mic as this is validated by the network-server
				rjPHYBytes, err := rjPHY.MarshalBinary()
				So(err, ShouldBeNil)

				rejoinReqPayload := backend.RejoinReqPayload{
					BasePayload: backend.BasePayload{
						ProtocolVersion: backend.ProtocolVersion1_0,
						SenderID:        "010203",
						ReceiverID:      "0807060504030201",
						TransactionID:   1234,
						MessageType:     backend.RejoinReq,
					},
					MACVersion: "1.1.0",
					PHYPayload: backend.HEXBytes(rjPHYBytes),
					DevEUI:     d.DevEUI,
					DevAddr:    lorawan.DevAddr{1, 2, 3, 4},
					DLSettings: lorawan.DLSettings{
						RX2DataRate: 5,
						RX1DROffset: 1,
					},
					RxDelay: 1,
					CFList:  backend.HEXBytes(cFListB),
				}
				rejoinReqPayloadJSON, err := json.Marshal(rejoinReqPayload)
				So(err, ShouldBeNil)

				req, err := http.NewRequest("POST", server.URL, bytes.NewReader(rejoinReqPayloadJSON))
				So(err, ShouldBeNil)

				resp, err := http.DefaultClient.Do(req)
				So(err, ShouldBeNil)
				So(resp.StatusCode, ShouldEqual, http.StatusOK)

				Convey("Then the expected response is returned", func() {
					var rejoinAnsPayload backend.RejoinAnsPayload
					So(json.NewDecoder(resp.Body).Decode(&rejoinAnsPayload), ShouldBeNil)
					So(rejoinAnsPayload, ShouldResemble, backend.RejoinAnsPayload{
						BasePayload: backend.BasePayload{
							ProtocolVersion: backend.ProtocolVersion1_0,
							SenderID:        "0807060504030201",
							ReceiverID:      "010203",
							TransactionID:   1234,
							MessageType:     backend.RejoinAns,
						},
						Result: backend.Result{
							ResultCode: backend.Success,
						},
						SNwkSIntKey: &backend.KeyEnvelope{
							AESKey: []byte{149, 120, 5, 96, 155, 128, 176, 208, 48, 95, 243, 111, 106, 151, 199, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						FNwkSIntKey: &backend.KeyEnvelope{
							AESKey: []byte{116, 107, 49, 181, 151, 129, 9, 167, 149, 189, 41, 47, 222, 74, 230, 204, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						NwkSEncKey: &backend.KeyEnvelope{
							AESKey: []byte{84, 195, 55, 234, 72, 154, 106, 145, 52, 17, 173, 15, 100, 144, 102, 197, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						AppSKey: &backend.KeyEnvelope{
							AESKey: []byte{25, 22, 171, 213, 146, 40, 141, 222, 213, 179, 62, 103, 11, 138, 46, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
						},
						PHYPayload: backend.HEXBytes([]byte{32, 158, 25, 130, 237, 63, 21, 29, 185, 20, 125, 135, 76, 181, 160, 229, 55, 207, 149, 204, 168, 149, 200, 232, 20, 95, 124, 58, 37, 198, 121, 230, 229}),
					})
				})
			})
		})
	})
}
